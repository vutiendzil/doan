@extends('layouts.app')

@section('content')
<div class="container">
<h3>Tạo bài viết mới</h3>
<form action="{{ route('article.store') }}" enctype='multipart/form-data'  method="post">
@csrf
    <div class="form-group">
        <label for="title" class="form-label">Tên bài viết : </label>
        <input type="text" name='title' id='title' class="form-control">
    </div>
    <div class="form-group">
        <label for="content" class="form-label">Nội dung bài viết : </label>
       <textarea name="content" id="content" class='form-control' cols="30" rows="5"></textarea>
    </div>
    <div class="form-group">
        <label for="img" class="form-label">Ảnh minh hoạ : </label>
        <input type="file" name='img' id='img' class="form-control">
    </div>
    <div class="form-group">
        <label for="author" class="form-label">Tác giả : </label>
        <input type="text" name='author' id='author' class="form-control">
    </div>
    <div class="form-group">
        <button type='submit' class='btn btn-primary'>Tải Bài Viết</button>
    </div>
</form>
</div>
@endsection