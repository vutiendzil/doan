<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$role)
    {
        
        if(Auth::user() -> role != $role) {
            return redirect()->route('article.index');
            // abort('khong dc truy cap',503);
            // echo Auth::user()->role;
        }
        
        return $next($request);
    }
}
